import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

//读取存入的parquet
object ReadParquet {
  def main(args: Array[String]): Unit = {
    // 停用log4J中的日志配置
    Logger.getLogger("org").setLevel(Level.WARN)
    val sparkSession = SparkSession.builder()
      .appName(this.getClass.getSimpleName)
      .master("local[*]")
      .getOrCreate()

    val dataFrame = sparkSession.read.parquet("data/idmp/2020-01-11")
    dataFrame.show(20, false)
  }
}
