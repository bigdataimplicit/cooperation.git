package pub.ryan.commons.util

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object SparkUtil {
  def getSparkSession(appName:String = "app", master:String = "local[*]", confMap:Map[String, String]=Map.empty): SparkSession ={
    val conf = new SparkConf()
    conf.setAll(confMap)
    SparkSession.builder()
      .appName(appName)
      .master(master)
      .getOrCreate()
  }
}
