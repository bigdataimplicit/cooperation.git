# cooperation

#### 介绍
{
     cooperation是一个基于用户日志行为，业务数据为基础的数仓系统，在此基础上通过数仓给用户画像，通过用户画像设计相应的推荐系统的综合项目。此项目是学习涛哥大数据综合项目的学习项目。
}


#### 软件架构
本项目划分为五个模块，分别是：
    <modules>
        <module>Commons：         基础工具库</module>
        <module>Dataware：        数据仓库</module>
        <module>Userprofile：     用户画像</module>
        <module>Recommend：       推荐系统</module>
        <module>co_web：          Web展示</module>
    </modules>

![软件架构](https://images.gitee.com/uploads/images/2020/1111/143933_6e1c8898_8258367.png "屏幕截图.png")


#### 使用技术

项目中所涉及到的技术：

采集：

	flume ： 分布式日志数据汇聚

	sqoop ： 离线批量抽取数据库

	cannal ： 实时数据库数据逐条监听、抽取

	nginx插件：前端


存储： 

	hdfs

        mysql

	redis

        hbase/elastic search ：用户画像标签数据的存储查询服务

        kafka ： 实时计算的缓冲

运算：

	hive	/ mapreduce
	
        spark core/ sql 

        flink / spark streaming

        OLAP引擎

        Presto


数仓元数据管理（血缘管理）：

Atlas


job调度系统：

Azkaban



算法（用户画像/推荐系统）：

spark代码/sql代码实现的通用统计计算；

	图计算： SparkGraphx；

	机器学习算法：

		KNN  k近邻

 		KMEANS   k均值

		Naive Bayes朴素贝叶斯

                Logistic Regression 逻辑回归

		随机森林

		协同过滤算法

		各类相似度算法：欧几里得距离、余弦相似度、皮尔逊相关系数

		NLP算法：

                TF-IDF 关键词提取

                文本向量化算法

                文本相似度算法

                文本分类算法

                关联规则分析：FP-GROWTH算法/APRIORI算法

